import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  public idProctud:any;
  public newProduct= new FormGroup({
    name:new FormControl('',[Validators.required]),
    cantidad:new FormControl('',[Validators.required]),
    precio:new FormControl('',[Validators.required]),
    dateExpirit:new FormControl('',[Validators.required]),
    state:new FormControl('',[Validators.required])
   });

  constructor(private _services:AppService, private _router:Router, private _active:ActivatedRoute) { }

  ngOnInit(): void {
       this.idProctud = this._active.snapshot.params.id;
     console.log(this.idProctud);

    if(this.idProctud){

    }




  }

  submitForm(){
    console.log(this.newProduct)
   if(this.newProduct.status != "INVALID"){
        this._services.postProduct(this.newProduct.value).then(res=>{
           this._router.navigate(['list'])

        }).catch(err=>{

        })


   }else{
     console.log('campo requerido no esta llenado');

   }


  }


}
