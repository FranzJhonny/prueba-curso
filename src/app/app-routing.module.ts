import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormularioComponent } from './formulario/formulario.component';
import { HeaderComponent } from './header/header.component';
import { ListaComponent } from './lista/lista.component';

const routes: Routes = [
  {path:'',redirectTo:'list' , pathMatch:'full'},

  {path:'',component: HeaderComponent,
    children:[
      {path:'form',component:FormularioComponent},
      {path:'form/:id',component:FormularioComponent},
      {path:'list', component:ListaComponent}
    ]
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
