import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AppService {
   public url ='https://6103959e79ed68001748255d.mockapi.io/Api/almacen/';
  constructor(private _http:HttpClient) { }

     getProducts(): Promise<any>{
       return this._http.get(this.url+'products').toPromise();
     }
     getOneProduct(id:any) : Promise<any>{
      return this._http.get(this.url+'products/' +id).toPromise();
    }

     deleteProduct(id:any): Promise<any>{
       return this._http.delete(this.url+'products/'+id).toPromise();
     }

     postProduct(newProduct:any): Promise<any>{
       return this._http.post(this.url+'products',newProduct).toPromise();
     }




}
