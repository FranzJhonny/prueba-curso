import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AppService } from '../app.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  public productos:any;
  constructor(private _service:AppService,private _router:Router) {

   }

  ngOnInit(): void {
this.getProducts();
  }

  getProducts(){
    this._service.getProducts().then(res=>{

      res.map((p:any)=>{
        p.labelEstato= p.state == false ? 'Inactivo' : 'Activo';
        p.dateExperit = moment(p.dateExperit).utc().format('DD/MM/YYYY');
        /* console.log(p); */
      })

      this.productos=res;


    })

  }

  deleteProduct(id:any){
    this._service.deleteProduct(id).then(res=>{
     console.log(res);
     this.getProducts();
    }).catch(err=>{
      console.log('no se puedo elinar por ya no esta');

    })
  }

  editProduct(id:any){
         this._router.navigate(['form/'+id])
  }


}
