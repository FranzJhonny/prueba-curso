import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
public productos:any;
  constructor(private _servicio:AppService) {


  }

  ngOnInit(): void {
this._servicio.getProducts().then(res=>{
this.productos=res;
 console.log(res);


})

  }

}
